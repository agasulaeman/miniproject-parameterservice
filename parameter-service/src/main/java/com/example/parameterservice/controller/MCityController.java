package com.example.parameterservice.controller;

import com.example.parameterservice.dto.MCityRequestDto;
import com.example.parameterservice.dto.MCityResponseDto;
import com.example.parameterservice.models.MCityModel;
import com.example.parameterservice.service.MCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/city")
public class MCityController {
    @Autowired
    private MCityService mCityService;

    @PostMapping
    public MCityResponseDto add(@RequestBody MCityRequestDto cityRequestDto) throws Exception {
        return mCityService.add(cityRequestDto);
    }

    @PutMapping(value = "/{id}")
    public MCityResponseDto update(@PathVariable(value = "id") Long id, @RequestBody MCityRequestDto cityRequestDto) throws Exception {
        return mCityService.update(id, cityRequestDto);
    }

    @DeleteMapping("/{id}")
    public String deleteCity(@PathVariable("id") Long id) throws Exception {
        mCityService.deleteCity(id);
        return "sukses merubah value is_Delete menjadi true";
    }

/*    @GetMapping
    public Iterable<MCityResponseDto> findAll(boolean isDeleted){
        return mCityService.findAll(isDeleted);
        */

  @GetMapping //(value = "/{id}")
    public Iterable<MCityModel> findAll(){
        return mCityService.findAll();
}
}
