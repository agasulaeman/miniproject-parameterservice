package com.example.parameterservice.repository;

import com.example.parameterservice.models.MCityModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MCityRepository extends CrudRepository<MCityModel,Long> {

}
