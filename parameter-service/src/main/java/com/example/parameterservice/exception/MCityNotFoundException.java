package com.example.parameterservice.exception;

public class MCityNotFoundException extends Exception{
    public MCityNotFoundException() {
        super();
    }

    public MCityNotFoundException (String message){
        super(message);
    }
}
