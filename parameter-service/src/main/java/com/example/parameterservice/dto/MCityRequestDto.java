package com.example.parameterservice.dto;

import lombok.Data;

@Data
public class MCityRequestDto {
    private String name;
    private String description;
}
