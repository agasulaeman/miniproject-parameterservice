package com.example.parameterservice.dto;

import lombok.Data;

@Data
public class MCityResponseDto {
    private Long id;
    private String name;
    private String description;
}
