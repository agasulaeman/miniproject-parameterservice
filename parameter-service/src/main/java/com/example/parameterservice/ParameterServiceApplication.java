package com.example.parameterservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ParameterServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParameterServiceApplication.class, args);
	}

}
