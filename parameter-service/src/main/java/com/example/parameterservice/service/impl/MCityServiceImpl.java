package com.example.parameterservice.service.impl;

import com.example.parameterservice.dto.MCityRequestDto;
import com.example.parameterservice.dto.MCityResponseDto;
import com.example.parameterservice.models.MCityModel;
import com.example.parameterservice.repository.MCityRepository;
import com.example.parameterservice.service.MCityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class MCityServiceImpl implements MCityService {

    @Autowired
    private MCityRepository mCityRepository;

    @Autowired public EntityManager entityManager;


    @Transactional
    @Override
    public MCityResponseDto add(MCityRequestDto cityRequestDto) throws Exception {
        MCityModel mCityModel = new MCityModel();
        mCityModel.setName(cityRequestDto.getName());
        mCityModel.setDescription(cityRequestDto.getDescription());

        MCityModel mCityRepo = mCityRepository.save(mCityModel);

        MCityResponseDto mCityResponse = new MCityResponseDto();
        mCityResponse.setId(mCityRepo.getId());
        mCityResponse.setName(mCityRepo.getName());
        mCityResponse.setDescription(mCityRepo.getDescription());

        return mCityResponse;
    }

    @Transactional
    @Override
    public MCityResponseDto update(Long id, MCityRequestDto cityRequestDto) throws Exception {
        Optional<MCityModel> findCity = mCityRepository.findById(id);
        if (findCity == null) {
            throw new Exception("ID City not Found");
        }
        findCity.get().setName(cityRequestDto.getName());
        findCity.get().setDescription(cityRequestDto.getDescription());

        MCityModel mCityModelSave = mCityRepository.save(findCity.get());

        MCityResponseDto cityResponseDto = new MCityResponseDto();
        cityResponseDto.setId(mCityModelSave.getId());
        cityResponseDto.setName(mCityModelSave.getName());
        cityResponseDto.setDescription(mCityModelSave.getDescription());

        return cityResponseDto;
    }

    @Transactional
    @Override
    public void deleteCity(Long id) throws Exception {
       Optional<MCityModel> deleteCityId = mCityRepository.findById(id);
        if (deleteCityId.isEmpty()) {
            throw new Exception("Can't Delete City, ID Not found");
        }
        log.info("id delete {} ", "sukses");
        mCityRepository.deleteById(id);
    }


    @Override
    public Iterable<MCityModel> findAll() {

      return mCityRepository.findAll();
        /*
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedMCityFilter");
        filter.setParameter("delete",isDeleted);


        Iterable<MCityModel> findByCity = mCityRepository.findAll();
        session.disableFilter("deletedMCityFilter");


        List<MCityResponseDto> cityResponseDtos = new ArrayList<>();
        findByCity.forEach(call->{
            MCityResponseDto cityResponseDto = new MCityResponseDto();
            cityResponseDto.setId(call.getId());
            cityResponseDto.setName(call.getName());
            cityResponseDto.setDescription(call.getDescription());

            cityResponseDtos.add(cityResponseDto);
        });
        */

      //  return cityResponseDtos;
    }
}
