package com.example.parameterservice.service;

import com.example.parameterservice.dto.MCityRequestDto;
import com.example.parameterservice.dto.MCityResponseDto;
import com.example.parameterservice.models.MCityModel;

public interface MCityService {
    MCityResponseDto add (MCityRequestDto cityRequestDto) throws Exception;
    MCityResponseDto update(Long id,MCityRequestDto cityRequestDto) throws Exception;
    void deleteCity(Long id) throws Exception;
    Iterable<MCityModel> findAll();
}
