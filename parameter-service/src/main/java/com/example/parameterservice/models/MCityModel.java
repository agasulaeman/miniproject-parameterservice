package com.example.parameterservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "m_city") // melakukan pembuatan table via Intellij Java agar langsung tercreate di databse db.city
@SQLDelete(sql = "UPDATE m_city SET is_deleted = true where id=?")
//@Where(clause = "isDeleted=false")
@FilterDef(name = "deletedMCityFilter", parameters =
@ParamDef(name = "delete", type = "boolean"))
@Filter(name = "deletedMCityFilter", condition = "isDeleted = :delete")

public class MCityModel {

    @Id // atas variable id merupakan primaryKey
    @GeneratedValue(strategy = GenerationType.IDENTITY)// yang merupakan untuk variable ini adalah auto increment
    @Column(name = "id",nullable = false,length = 50) //pembuatan column  dan untuk length atau panjang karakter
    private Long id;

    @Column(name = "name",nullable = false,length = 100)// pembuatan column name dan length pada column tsb
    private String name;

    @Column(name = "description",nullable = false,length = 100)//pembuatan column description dan length pada column tsb
    private  String description;

    private Boolean isDeleted = Boolean.FALSE; //memberikan value nilai bahwa boolean isDeleted = false

/*
    private MCityStatusRecord mCityStatusRecord = MCityStatusRecord.ACTIVE;

    private String createBy;

    private String updateBy;

    private LocalDateTime createdDate;

    private LocalDateTime updateDateTime;

*/

}
